import React from 'react'
import { Route, IndexRoute } from 'react-router'
import Theme from './Theme'

export const createRoutes = (store) => {
  return (
    <Route>
      <IndexRoute component={Theme} />
      <Route path='*' component={Theme} />
    </Route>
  )
}

export default createRoutes
