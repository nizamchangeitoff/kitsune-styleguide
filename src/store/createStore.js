import { applyMiddleware, compose, createStore as createReduxStore } from 'redux'
import thunk from 'redux-thunk'
import { browserHistory } from 'react-router'
import makeRootReducer from '../reducers/rootReducers'
import { updateLocation } from '../reducers/location'
import { createLogger } from 'redux-logger'
import { devToolsEnhancer } from 'redux-devtools-extension/logOnlyInProduction'



const logger = createLogger({})


const createStore = (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  let middleware = [thunk]

  // ======================================================
  // Store Enhancers
  // ======================================================
  let enhancers = []
  let composeEnhancers = compose

  if (__DEV__) {
    enhancers.push(devToolsEnhancer())
    middleware.push(logger)
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createReduxStore(
    makeRootReducer(),
    initialState,
    composeEnhancers(
      applyMiddleware(...middleware),
      ...enhancers
    ),
  )

  store.asyncReducers = {}

  // To unsubscribe, invoke `store.unsubscribeHistory()` anytime
  store.unsubscribeHistory = browserHistory.listen(updateLocation(store))

  if (module.hot) {
    module.hot.accept('../reducers/rootReducers', () => {
      const reducers = require('./../reducers/rootReducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}

export default createStore
