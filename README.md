# kitsune styleguide

## Requirements

* Nodejs > 6.10

## Installation


```
    $ npm install

```

or faster **yarn**

```
    $ npm install -g yarn
    $ yarn install

```


## To run

Read package.json for scripts provided.yarn
To start simply type.

```
    $ yarn start
```